<?php $v->layout("_layout"); ?>

<div class="container">
    <?php $v->insert("jumbotron", ["title" => null]); ?>
    <div class="row">
        <div class="col">
            <a href="<?=$router->route("web.create");?>"
               class="btn btn-primary mb-2">Adicionar novo</a>
            <?php if (!$users): ?>
                <div class="alert alert-info">Nenhum registro encontrado</div>
            <?php else: ?>
            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Editar</th>
                    <th scope="col">Deletar</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user): ?>
                    <tr>
                        <th scope="row"><?=$user->id?></th>
                        <td><?=$user->name?></td>
                        <td><?=$user->email?></td>
                        <td>
                            <a href="<?=$router->route("web.edit", ["id" => $user->id]);?>"
                               class="btn btn-warning">Editar</a>
                        </td>
                        <?php if ($user->id !== $userId): ?>
                        <td>
                            <a onclick="return confirm('Deseja realmente deletar?');" href="<?=$router->route("web.destroy", ["id" => $user->id]);?>"
                               class="btn btn-danger">Deletar</a>
                        </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
        </div>
    </div>
</div>
