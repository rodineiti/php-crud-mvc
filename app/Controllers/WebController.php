<?php

namespace App\Controllers;

use App\Models\User;
use App\Support\Session;

class WebController extends Controller
{
    public function __construct($router)
    {
        parent::__construct($router);

        if (!Session::has("user")) {
            $this->router->redirect("web.loginForm");
        }
    }

    public function index()
    {
        $users = (new User())->find()->fetch(true);

        echo $this->view->render("home", [
            "users" => $users,
            "userId" => Session::get("user")
        ]);
    }

    public function create()
    {
        echo $this->view->render("create");
    }

    public function store(array $data)
    {
        $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

        if (!empty($data["name"]) && !empty($data["email"]) && !empty($data["password"])) {
            $user = new User();
            $user->name = $data["name"];
            $user->email = $data["email"];
            $user->password = $data["password"];

            if (!$user->save()) {
                setFlashMessage("danger", $user->fail()->getMessage());
                $this->router->redirect("web.create");
            }

            setFlashMessage("success", "Criado com sucesso");
            $this->router->redirect("web.index");

        } else {
            setFlashMessage("danger", "Favor informar o nome e o e-mail");
            $this->router->redirect("web.create");
        }
    }

    public function edit($data)
    {
        $id = filter_var($data["id"], FILTER_VALIDATE_INT);

        if (!$id) {
            setFlashMessage("danger", "Favor informar o ID");
            $this->router->redirect("web.index");
        }

        $user = (new User())->findById($id);

        if (!$user) {
            setFlashMessage("danger", "Nenhum usuário encontrado com este ID");
            $this->router->redirect("web.index");
        }

        echo $this->view->render("edit", [
            "user" => $user
        ]);
    }

    public function update(array $data)
    {
        $id = filter_var($data["id"], FILTER_VALIDATE_INT);
        $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

        if (!$id) {
            setFlashMessage("danger", "Favor informar o ID");
            $this->router->redirect("web.index");
        }

        $user = (new User())->findById($id);

        if (!$user) {
            setFlashMessage("danger", "Nenhum usuário encontrado com este ID");
            $this->router->redirect("web.index");
        }

        if (!empty($data["name"]) && !empty($data["email"])) {
            $user->name = $data["name"];
            $user->email = $data["email"];
            $user->password = $data["password"];

            if (!$user->save()) {
                setFlashMessage("danger", $user->fail()->getMessage());
                $this->router->redirect("web.edit", ["id" => $user->id]);
            }

            setFlashMessage("success", "Atualizado com sucesso");
            $this->router->redirect("web.index");

        } else {
            setFlashMessage("danger", "Favor informar o nome e o e-mail");
            $this->router->redirect("web.edit", ["id" => $user->id]);
        }
    }

    public function destroy($data)
    {
        $id = filter_var($data["id"], FILTER_VALIDATE_INT);

        if (!$id) {
            setFlashMessage("danger", "Favor informar o ID");
            $this->router->redirect("web.index");
        }

        $user = (new User())->findById($id);

        if (!$user) {
            setFlashMessage("danger", "Nenhum usuário encontrado com este ID");
            $this->router->redirect("web.index");
        }

        if ($user->id === Session::get("user")) {
            setFlashMessage("danger", "Você não pode deletar seu próprio usuário");
            $this->router->redirect("web.index");
        }

        $user->destroy();

        setFlashMessage("success", "Deletado com sucesso");
        $this->router->redirect("web.index");
    }
}