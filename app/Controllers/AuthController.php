<?php

namespace App\Controllers;

use App\Models\User;
use App\Support\Session;

class AuthController extends Controller
{
    public function __construct($router)
    {
        parent::__construct($router);
    }

    public function loginForm()
    {
        if (Session::has("user")) {
            $this->router->redirect("web.index");
        }

        echo $this->view->render("login");
    }

    public function login(array $data)
    {
        $email = filter_var($data["email"], FILTER_VALIDATE_EMAIL);
        $password = filter_var($data["password"], FILTER_DEFAULT);

        if ($email && $password) {

            $user = (new User())->find("email = :email", "email={$email}")->fetch();

            if (!$user) {
                setFlashMessage("danger", "Dados inválidos, e-mail ou senha inválidos");
                $this->router->redirect("web.loginForm");
            }

            if (!password_verify($password, $user->password)) {
                setFlashMessage("danger", "Dados inválidos, e-mail ou senha inválidos");
                $this->router->redirect("web.loginForm");
            }

            Session::set("user", $user->id);

            $this->router->redirect("web.index");

        } else {
            setFlashMessage("danger", "Favor informar o nome e o e-mail");
            $this->router->redirect("web.loginForm");
        }
    }

    public function logout()
    {
        Session::destroy("user");
        setFlashMessage("info", "Você fez logout com sucesso.");
        $this->router->redirect("web.loginForm");
    }
}